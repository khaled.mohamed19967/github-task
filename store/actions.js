import {UserRepos} from "@/apis/repos";

export const actions = {
  async signIn({commit}, userName) {
    try {
      const res = await this.$axios.get(`${userName}/${UserRepos.repos}`);
      commit('SET_USER_INFO', res.data[0].owner);
      this.$cookies.set('isLoggedIn', true);
      return true;
    } catch(err) {
      this._vm.$bvToast.toast('User name is invalid', {
        title: `Check`,
        solid: true,
        variant: 'danger',
      })
      return false;
    }
  },
  signOut({commit}) {
    commit('SET_USER_LOG_OUT');
  }
}
export default actions;
