export const getters = {
  getLoggedIn: (state) => state.loggedIn,
}
export default getters;
