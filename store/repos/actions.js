import { UserRepos } from "~/apis/repos";

export const actions = {
  async userRepo({commit}, userName) {
    try {
      let res = await this.$axios.get(`${userName}/${UserRepos.repos}`);
      commit('SET_USER_REPOS', res.data);
    } catch(err) {}
  }
}
export default actions;
