export const mutations = {
  SET_USER_LOGGED_IN(state, payload) {
    state.loggedIn = payload;
  },
  SET_USER_INFO(state, payload) {
    state.userInfo = payload;
  },
  SET_USER_LOG_OUT(state) {
    this.$cookies.set('isLoggedIn', false);
    window.location.href = '/login';
    state.userInfo = [];
  }
}
export default mutations;
