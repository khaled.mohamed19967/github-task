
const auth = ({app, redirect, store}) => {
  const ifUser = app.$cookies.get('isLoggedIn');
  if (!ifUser) {
    redirect('/login');
    store.state.userInfo = [];
  }
  else redirect('/dashboard');
};

export default auth;
